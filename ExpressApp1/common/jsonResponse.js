﻿'use strict';

function jsonData(isError, Data, errorMessage) {
    var data = {};
    if (isError) {
        data.status = 'ERROR';
        data.message = errorMessage;
        data.data = [];
        return data;
    } else {
        data.status = 'OK';
        data.message = 'OK';
        data.data = Data;
        return data;
    }
};

module.exports = jsonData;