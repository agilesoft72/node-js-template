﻿'use strict';
var debug = require('debug');

// ENV
require('dotenv').config();

// Express 기본 모듈
var express = require('express');
const hbs = require('express-handlebars');   // handlebars 템플릿 엔진
var path = require('path');
var favicon = require('serve-favicon');
var mongoose = require('mongoose');


var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var trace = require('./common/logger');

//var cors = require('cors'); // ajax 요청용 다줏서버 접속(CORS) 지원

// *****************************************************
// APP setting.
// *****************************************************
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'pug');
//** handlebars 핵심 설정 시작 **//
app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'main',    // 기본 레이아웃 파일 main.hbs 지정, 요청에 따라 레이아웃을 변경할수 있다.
    layoutsDir: __dirname + '/views/layouts/',  // 헨들바템플릿의 레이아웃 파일의 위치
    partialsDir: __dirname + '/views/partials/' // 파티셜이란: 레이아웃을 채울 header.hbs, left.hbs, footer.hbs 파일의 위치
}));

app.set('view engine', 'hbs'); // handlebars파일의 확장자를 hbs로 사용.


// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSession({
    secret: 'my session key',
    resave: true,
    saveUninitialized: true    
}));
app.use(express.static(path.join(__dirname, 'public')));
// 파일 업로드 폴더
app.use(express.static(path.join(__dirname, 'uploads')));
// 업로드 폴더 보기 설정.
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

// multer 미들웨어 사용 순서 중요 : body-parser -> multer -> router
var multer = require('multer');
var fs = require('fs');
// *****************************************************
// 사용자 모듈 
// *****************************************************
var trace = require('./common/logger');
var routes = require('./routes/index');
var users = require('./routes/users');
var sessionCookieTest = require('./routes/SessionCookieTest');
var upload = require('./routes/upload');

app.use('/', routes);
app.use('/users', users);
app.use('/session', sessionCookieTest);
app.use('/upload', upload);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);

// 프로세스 종료 시에 데이터베이스 연결 해제
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true  })
    .then(() => trace.info('데이타베이스 연결 성공'))
    .catch(e => trace.error(e));



//console.dir(process.env);

var server = app.listen(app.get('port'), function () {
    trace.info('서버가 시작되었습니다. 포트:%s', app.get('port'));   
});
