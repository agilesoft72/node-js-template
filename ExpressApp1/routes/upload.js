﻿'use strict';
var express = require('express');
var dt = require('date-utils');
var router = express.Router();
var multer = require('multer');
var trace = require('../common/logger');
var mongoose = require('mongoose');
var jsonData = require('../common/jsonResponse');

// 파일업로드 관련 Setting
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'uploads');
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname)
        //callback(null, Date.now() + '_' + file.originalname)
    }
});
// 파일제한 10개 , 1G
var upload = multer({
    storage: storage,
    limits: {
        files: 10,
        fileSize: 1024 * 1024 * 1024
    }
});

// Mongo schema
var fileSchema = mongoose.Schema({ filename: String, datetime: String, version: String });
var FileModel = mongoose.model("FileModel", fileSchema);


router.get('/', function (req, res) {
    res.render('upload', { title: '파일전송' });
});


// 파라미터 : multer.single('폼 파일 name')
router.post('/upload', upload.array('files') ,function (req, res) {
    trace.info('/upload 호출');
    var paramVersion = req.body.version;

    try {
        var files = req.files;
        var OriginalName = '',
            filename = '',
            mimetype = '',
            size = 0;
        var dt = new Date().toFormat('YYYY-MM-DD HH24:MI:SS');

        if (Array.isArray(files)) {
            for (var index = 0; index < files.length; index++) {
                OriginalName = files[index].originalname;
                filename = files[index].filename;
                mimetype = files[index].mimetype;
                size = files[index].size;

                // mongo DB Save
                var fileIns = new FileModel({ filename: filename, datetime: dt, version: paramVersion });
                fileIns.save(function (err, fileIns) {
                    if (err) return trace.error(err);
                    trace.info(fileIns);
                });

                trace.info('#upload : %s', filename);
            }
        }
    }
    catch (exception) {
        trace.error(exception);
    }

    //res.render('uploaddone', { filename: filename, originalfilename: OriginalName });
    res.send('업로드 완료');
});

router.get('/view', function (req, res) {
    var version = req.query.version;

    if (version) {
        FileModel.find({ version: version }, function (err, data) {            
            if (err) return res.status(500).send(jsonData(true, [], err));
            if (!data) return res.status(404).send(jsonData(true, [], err));
            
            res.status(200).send(jsonData(false, data, ''));
        });
    } else {
        FileModel.find({}, function (err, data) {            
            if (err) return res.status(500).send(jsonData(true, [], err));            
            if (!data) return res.status(404).send(jsonData(true, [], err));
            
            res.status(200).send(jsonData(false, data, ''));
        });
    }
});

//router.delete('/:version', function (req, res) {
router.get('/delete', function (req, res) {
    var version = req.query.version;

    FileModel.deleteMany({ version: version }, function (err, data) {
        if (err) return res.status(500).send(jsonData(true, [], err));
        if (!data) return res.status(404).send(jsonData(true, [], err));

        res.status(200).send(jsonData(false, [], ''));
    });

});


//// User 수정
//router.put('/:id', function (req, res) {
//    User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, user) {
//        if (err) return res.status(500).send("User 수정 실패.");
//        res.status(200).send(user);
//    });
//});


module.exports = router;