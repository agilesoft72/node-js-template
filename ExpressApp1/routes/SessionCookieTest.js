﻿'use strict';
var express = require('express');
var router = express.Router();
var trace = require('../common/logger');

/* Cookie Test */
router.get('/setcookie', function (req, res) {
    res.cookie('user', {
        id: 'cookie_id',
        name: '홍길동',
        authorized: true
    });
    trace.info('setcookie', req);

    res.redirect(200, '/showcookie');

});

router.get('/showcookie', function (req, res) {
    trace.info('showcookie 호출:', req.cookies);
    res.send(req.cookies);
});

/* express-session Test */
router.get('/login', function (req, res) {
    trace.info('login 호출');
    if (req.session.user) {
        trace.info('이미 세션이 있음.');
    } else {
        req.session.user = {
            id: 'test_session_id',
            name: '홀갈동',
            authorized: true
        };

        trace.info('신규 로그인 세션생성');
        req.session.save(function () {
            //res.redirect('/');
        });

    }
});

router.get('/checklogin', function (req, res) {
    if (req.session.user) {
        trace.info('이미로그인 되어 있습니다.');
    } else {
        trace.info('로그인 되어 있지 않습니다.');
    }
});

router.get('/logout', function (req, res) {
    if (req.session.user) {
        delete req.session.user;
        req.session.save(function () {
            //res.redirect('/');
        });

        trace.info('로그아웃 되었습니다.');

    }
});

module.exports = router;
